import argparse
import heapq
import json
import os
import posixpath
import re
import sys
from datetime import datetime, timezone

import boto3


class S3Uploader:
    def __init__(
        self, aws_access_key_id, aws_secret_access_key, destination_folder, bucket_name
    ):
        self.s3 = boto3.client(
            "s3",
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
        )
        self.destination_folder = destination_folder

    def upload_file(self, file_path, bucket_name):
        file_name = os.path.basename(file_path)
        # Validate file name format
        if not self.validate_file_name(file_name):
            raise ValueError("File name does not match the required format.")

        # Extract NORAD ID from the file name
        norad_id = self.get_norad(file_name)

        # Upload file to S3
        s3_key = posixpath.join(self.destination_folder, norad_id, file_name)
        self.s3.upload_file(file_path, bucket_name, s3_key)

    def download_file(self, file_path, bucket_name):
        output_path = os.path.join(os.getcwd(), os.path.basename(file_path))
        self.s3.download_file(bucket_name, file_path, output_path)

    def list_files(self, norad_id, bucket_name, today=False, uploads=False):
        prefix = f"{self.destination_folder}{norad_id}"
        paginator = self.s3.get_paginator("list_objects_v2")
        response_iterator = paginator.paginate(Bucket=bucket_name, Prefix=prefix)

        all_files = []
        for page in response_iterator:
            if "Contents" in page:
                all_files.extend(page["Contents"])

        files_sorted = list(
            sorted(all_files, key=lambda obj: obj["LastModified"], reverse=True)
        )
        files_sorted = list(filter(lambda x: not x["Key"].endswith("/"), files_sorted))
        if uploads:
            files_sorted = list(
                filter(lambda x: not "output" in x["Key"], files_sorted)
            )
        else:
            files_sorted = list(filter(lambda x: "output" in x["Key"], files_sorted))
        if today:
            files_sorted = list(
                filter(
                    lambda x: x["LastModified"]
                    > datetime.combine(
                        datetime.now(timezone.utc).date(),
                        datetime.min.time(),
                    ).replace(tzinfo=timezone.utc),
                    files_sorted,
                )
            )
        return [obj["Key"] for obj in files_sorted]

    def validate_file_name(self, file_name):
        format_1 = (
            r"gnss_(\d{9}|\d{5})_\d{4}_\d{2}_\d{2}_\d{2}_\d{2}_\d{2}Z\."
            r"(json|csv|navsol)"
        )
        format_2 = (
            r"gnss_(\d{9}|\d{5})_\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z\."
            r"(json|csv|navsol)"
        )
        format_3 = (
            r"maneuver_(\d{9}|\d{5})_\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z\."
            r"(opm|txt)"
        )
        format_4 = (
            r"maneuver_(\d{9}|\d{5})_\d{4}_\d{2}_\d{2}_\d{2}_\d{2}_\d{2}Z\."
            r"(opm|txt)"
        )
        return (
            bool(re.match(format_1, file_name))
            or bool(re.match(format_2, file_name))
            or bool(re.match(format_3, file_name))
            or bool(re.match(format_4, file_name))
        )

    def get_norad(self, file_name):
        parts = file_name.split("_")
        if len(parts) >= 2:
            return parts[1]
        raise ValueError("File name does not match the required format")

    def _download_files(self, norad_id, bucket_name, num_files, include_pdf=False):
        prefix = f"{self.destination_folder}{norad_id}/output/"
        paginator = self.s3.get_paginator("list_objects_v2")
        response_iterator = paginator.paginate(Bucket=bucket_name, Prefix=prefix)

        definitive_count = 0
        predictive_count = 0
        pdf_downloaded = False

        file_heap = []

        for page in response_iterator:
            if "Contents" not in page:
                continue

            for file in page["Contents"]:
                if (
                    "DEFINITIVE" in file["Key"]
                    or "PREDICTIVE" in file["Key"]
                    or file["Key"].endswith(".pdf")
                ):
                    heapq.heappush(file_heap, (-file["LastModified"].timestamp(), file["Key"]))

        while file_heap:
            _, file_key = heapq.heappop(file_heap)
            file_name = os.path.basename(file_key)

            if (
                definitive_count == num_files
                and predictive_count == num_files
                and (pdf_downloaded or not include_pdf)
            ):
                break

            if file_name.endswith(".txt"):
                if "DEFINITIVE" in file_name and definitive_count < num_files:
                    self.download_file(file_key, bucket_name)
                    definitive_count += 1
                elif "PREDICTIVE" in file_name and predictive_count < num_files:
                    self.download_file(file_key, bucket_name)
                    predictive_count += 1
            elif include_pdf and file_name.endswith(".pdf") and not pdf_downloaded:
                self.download_file(file_key, bucket_name)
                pdf_downloaded = True

        return definitive_count, predictive_count, pdf_downloaded

    def download_recent(self, norad_id, bucket_name, num_files):
        definitive_count, predictive_count, _ = self._download_files(
            norad_id, bucket_name, num_files
        )
        print(
            f"Downloaded {definitive_count} DEFINITIVE files and {predictive_count} PREDICTIVE files"
        )

    def download_recent_report(self, norad_id, bucket_name, num_files):
        definitive_count, predictive_count, pdf_downloaded = self._download_files(
            norad_id, bucket_name, num_files, include_pdf=True
        )
        print(
            f"Downloaded {definitive_count} DEFINITIVE files and {predictive_count} PREDICTIVE files"
        )
        if pdf_downloaded:
            print("Downloaded the most recent PDF report file")
        else:
            print("No PDF report file found")


def main():
    parser = argparse.ArgumentParser(description="Upload file to S3")
    parser.add_argument(
        "file_path",
        help="Path to the file on the local filesystem if uploading or downloading; (temporary) NORAD ID if listing",
    )
    parser.add_argument(
        "--list",
        action="store_true",
        help="List files in the bucket ordered by their upload date in descending order.",
    )
    parser.add_argument(
        "--list-today",
        action="store_true",
        help="List files in the bucket ordered by their upload date in descending order whose upload date is after today UTC.",
    )
    parser.add_argument(
        "--list-uploads",
        action="store_true",
        help="List user-uploaded files in the bucket ordered by their upload date in descending order.",
    )
    parser.add_argument(
        "--list-uploads-today",
        action="store_true",
        help="List user-uploaded files in the bucket ordered by their upload date in descending order whose upload date is after today UTC.",
    )
    parser.add_argument("--download", action="store_true")
    parser.add_argument(
        "--download-recent",
        type=int,
        metavar="N",
        help="Download N recent DEFINITIVE and PREDICTIVE files for a given NORAD ID",
    )
    parser.add_argument(
        "--download-recent-report",
        type=int,
        metavar="N",
        help="Download N recent DEFINITIVE and PREDICTIVE files and the most recent PDF report file for a given NORAD ID",
    )
    parser.add_argument("--config-file", help="Path to the config file")
    args = parser.parse_args()

    # Check for config file
    config_file = args.config_file or "config.json"

    if not os.path.exists(config_file):
        print(f"Error: Config file not found at {config_file}", file=sys.stderr)
        sys.exit(1)

    # Read config file
    try:
        with open(config_file, "r") as f:
            config = json.load(f)
    except json.JSONDecodeError:
        print(f"Error: Invalid JSON in config file {config_file}", file=sys.stderr)
        sys.exit(1)
    except IOError:
        print(f"Error: Unable to read config file {config_file}", file=sys.stderr)
        sys.exit(1)

    # Extract credentials and destination folder from config
    aws_access_key_id = config.get("aws_access_key_id")
    aws_secret_access_key = config.get("aws_secret_access_key")
    destination_folder = config.get("destination_folder")
    bucket_name = config.get("bucket_name")

    if not all(
        [aws_access_key_id, aws_secret_access_key, destination_folder, bucket_name]
    ):
        print("Error: Missing required configuration in config file", file=sys.stderr)
        sys.exit(1)

    uploader = S3Uploader(
        aws_access_key_id, aws_secret_access_key, destination_folder, bucket_name
    )

    if args.download_recent:
        uploader.download_recent(args.file_path, bucket_name, args.download_recent)
    elif args.download_recent_report:
        uploader.download_recent_report(
            args.file_path, bucket_name, args.download_recent_report
        )
    elif args.download:
        uploader.download_file(args.file_path, bucket_name)
        print(
            f"File downloaded successfully from S3: {destination_folder}",
            file=sys.stderr,
        )
    elif args.list:
        try:
            print("\n".join(uploader.list_files(args.file_path, bucket_name)))
        except Exception as e:
            raise ValueError(
                f"Failed to list objects from the remote path ({e})."
            ) from None
    elif args.list_today:
        try:
            print(
                "\n".join(uploader.list_files(args.file_path, bucket_name, today=True))
            )
        except Exception as e:
            raise ValueError(
                f"Failed to list objects from the remote path ({e})."
            ) from None
    elif args.list_uploads:
        try:
            print(
                "\n".join(
                    uploader.list_files(args.file_path, bucket_name, uploads=True)
                )
            )
        except Exception as e:
            raise ValueError(
                f"Failed to list objects from the remote path ({e})."
            ) from None
    elif args.list_uploads_today:
        try:
            print(
                "\n".join(
                    uploader.list_files(
                        args.file_path, bucket_name, today=True, uploads=True
                    )
                )
            )
        except Exception as e:
            raise ValueError(
                f"Failed to list objects from the remote path ({e})."
            ) from None
    else:
        uploader.upload_file(args.file_path, bucket_name)
        print(
            f"File uploaded successfully to S3: {destination_folder}", file=sys.stderr
        )


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(f"Error: {e}", file=sys.stderr)
        sys.exit(-1)
