# Installation Instructions

1. Clone the Repository:
Clone the repository containing the script to your local machine:
`git clone https://github.com/your_username/your_repository.git`

2. Install Dependencies:
Navigate to the directory where the script is located and install the required dependencies using pip:
`pip install -r requirements.txt`

3. Prepare Configuration File:
Create a JSON configuration file (config.json) with the following structure:
```json
{
    "aws_access_key_id": "YOUR_ACCESS_KEY",
    "aws_secret_access_key": "YOUR_SECRET_KEY",
    "destination_folder": "ORG_NAME/",
    "bucket_name": "KAYHAN_REMOTE_BUCKET"
}
```

Replace `YOUR_ACCESS_KEY`, `YOUR_SECRET_KEY` with your AWS credentials, `ORG_NAME` with the Kayhan-provided organization name folder, and finally the `KAYHAN_REMOTE_BUCKET` with the Kayhan provided bucket name. Kayhan Space can also provide a pre filled-out `config.json` file which will replace the default file in the top-level directory. This `config.json` file will be used by default.

# Running the SDK

### Upload a file

Execute the script from the command line, providing the path to the file you want to upload. An optional `--config-file` parameter can be given if needed:

```bash
python kayhan_gnss_uploader.py /path/to/local/gnss_799123456_2023_02_28_12_33_44Z.json --config-file second_config_file.json
```

Replace `/path/to/local/gnss_799123456_2023_02_28_12_33_44Z.json` with the path to the file you want to upload.

---

### List files

Files in a spacecraft's output directory such as ephemeris can be listed using the `--list` option, or list recent files using `--list-today`. For example, to list outputs for object `123456789`:

```bash
python kayhan_gnss_uploader.py 123456789 --list
```

---

### Download files

Files in a spacecraft's output directory such as ephemeris can be downloaded to the current directory using the `--download` option. For example, to download the file `operator/123456789/MEME_TEST_2024-01-01.txt`:

```bash
python kayhan_gnss_uploader.py operator/123456789/output/MEME_TEST_2024-01-01.txt --download
```

---

### Download recent files

To download the most recent DEFINITIVE and PREDICTIVE files for a given NORAD ID, use the `--download-recent` option followed by the number of files to download for each type. For example, to download the 5 most recent DEFINITIVE and 5 most recent PREDICTIVE files for NORAD ID 123456789:

```bash
python kayhan_gnss_uploader.py 123456789 --download-recent 5
```

This will download up to 5 DEFINITIVE files and up to 5 PREDICTIVE files.

---

### Download recent report

To download the most recent DEFINITIVE and PREDICTIVE files along with the most recent report for a given NORAD ID, use the `--download-recent-report` option followed by the number of files to download for each type. For example, to download the 5 most recent DEFINITIVE and 5 most recent PREDICTIVE files, plus the most recent report PDF file for NORAD ID 123456789:

```bash
python kayhan_gnss_uploader.py 123456789 --download-recent-report 5
```

This will download up to 5 DEFINITIVE files, up to 5 PREDICTIVE files, and the most recent report PDF file (if any) to the current working directory.

---
